document.getElementById('login-form').addEventListener('submit', function(event) {
  event.preventDefault();
  
  const openaiKey = document.getElementById('openai-key').value;
  
  // TODO: Call the backend API to authenticate the user with the OpenAI key
  console.log('OpenAI Key:', openaiKey);
    document.getElementById('openai-auth-form').addEventListener('submit', function(event) {
      event.preventDefault();
      const openaiKey = document.getElementById('openai-key').value;
      // TODO: Call the backend API to authenticate the user with the OpenAI key
      console.log('OpenAI Key:', openaiKey);
      document.getElementById('chat-interface').classList.remove('hidden');
    });
    document.getElementById('send-message').addEventListener('click', function() {
      const model = document.getElementById('model-selection').value;
      if (model !== 'gpt-3.5' && model !== 'gpt-4.0') {
        console.error('Invalid model:', model);
        return;
      }
      const message = document.getElementById('chat-input').value;
      // TODO: Call the backend API to send the message and receive a response
      console.log('Model:', model, 'Message:', message);

      // Append the sent message to the chat history
      const chatHistory = document.getElementById('chat-history');
      const messageElement = document.createElement('p');
      messageElement.textContent = message;
      chatHistory.appendChild(messageElement);
    });
